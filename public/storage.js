document.addEventListener('DOMContentLoaded', () => {
    let savedFormData = localStorage.getItem('formData');  
    if (savedFormData) {                                   
      savedFormData = JSON.parse(savedFormData);           
      document.getElementById('name').value = savedFormData.name; 
      document.getElementById('phone').value = savedFormData.phone; 
      localStorage.removeItem('formData');                 
    }
  
    window.addEventListener('beforeunload', e => {         
      let nam = document.getElementById('name').value,   
          ph  = document.getElementById('phone').value; 
      if (nam.length || ph.length) {                     
        let formData = {                                   
          name: nam, 
          phone: ph
        };
        localStorage.setItem('formData', JSON.stringify(formData)); 
      }
    }); 
  });
